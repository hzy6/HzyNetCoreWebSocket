﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySocket.WebSocket.Core
{
    public class WebSocketMessageBase<TKey>
    {
        /// <summary>
        /// 标识 区分 当前 指令 是什么类型
        /// </summary>
        public TKey Key { get; set; }


    }
}
