﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySocket.WebSocket.Core
{
    public class WebSocketMessageResult<TKey>
    {
        public WebSocketMessageResult(TKey EKey)
        {
            this.EKey = EKey;
        }

        public WebSocketMessageResult(TKey EKey, object Data)
        {
            this.EKey = EKey;
            this.Data = Data;
        }

        private readonly TKey EKey;

        public string Key
        {
            get
            {
                return EKey.ToString();
            }
        }

        public object Data { get; set; }


    }
}
