﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySocket.WebSocket
{
    using HzySocket.WebSocket.Core;
    using System.Threading.Tasks;

    public class HzyWebSocketExtensions
    {



        /// <summary>
        /// 根据条件筛选 Session 对象
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IEnumerable<TSession> GetAppSessions<TSession>(Func<TSession, bool> predicate)
            where TSession : WebSocketSessionBase, new()
            => WebSocketSessionContainer<TSession>.GetAppSessions(predicate);

        /// <summary>
        /// 获取 所有 session 对象
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<TSession> GetAllAppSessions<TSession>()
            where TSession : WebSocketSessionBase, new()
            => WebSocketSessionContainer<TSession>.GetAllAppSessions();

        /// <summary>
        /// 插入Session 对象
        /// </summary>
        /// <param name="appSession"></param>
        public static void AddAppSession<TSession>(TSession appSession)
            where TSession : WebSocketSessionBase, new()
            => WebSocketSessionContainer<TSession>.AddAppSession(appSession);

        /// <summary>
        /// 移除Session 对象
        /// </summary>
        /// <param name="predicate"></param>
        public static void RemoveSession<TSession>(Func<TSession, bool> predicate)
            where TSession : WebSocketSessionBase, new()
            => WebSocketSessionContainer<TSession>.RemoveSession(predicate);

        /// <summary>
        /// 获取 session 连接数
        /// </summary>
        /// <returns></returns>
        public static int GetAppSessionCount<TSession>()
            where TSession : WebSocketSessionBase, new()
            => WebSocketSessionContainer<TSession>.GetAppSessionCount();

        /// <summary>
        /// 检查 session 是否存在
        /// </summary>
        /// <returns></returns>
        public static bool Any<TSession>(Func<TSession, bool> predicate)
            where TSession : WebSocketSessionBase, new()
            => WebSocketSessionContainer<TSession>.Any(predicate);

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="predicate"></param>
        /// <param name="Data"></param>
        public static async Task SendAsync<T, TSession>(Func<TSession, bool> predicate, T Data)
            where TSession : WebSocketSessionBase, new()
            => await WebSocketSessionContainer<TSession>.SendAsync(predicate, Data);

        /// <summary>
        /// 所有连接用户发送消息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Data"></param>
        public static async Task SendAllAsync<T, TSession>(T Data)
            where TSession : WebSocketSessionBase, new()
            => await WebSocketSessionContainer<TSession>.SendAllAsync(Data);




    }
}
