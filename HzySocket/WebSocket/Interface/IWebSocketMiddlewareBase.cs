﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySocket.WebSocket.Interface
{
    using System.Threading.Tasks;

    public interface IWebSocketMiddlewareBase<TSession>
    {
        /// <summary>
        /// 指令解析
        /// </summary>
        /// <param name="appSession"></param>
        /// <returns></returns>
        Task ExecuteCommand(TSession appSession);

    }
}
