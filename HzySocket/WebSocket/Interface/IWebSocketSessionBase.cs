﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySocket.WebSocket.Interface
{
    using Microsoft.AspNetCore.Http;
    using System.Net.WebSockets;
    using System.Threading;
    using System.Threading.Tasks;

    public interface IWebSocketSessionBase
    {
        /// <summary>
        /// 唯一 Id
        /// </summary>
        string SessionKey { get; set; }
        /// <summary>
        /// CancellationToken
        /// </summary>
        CancellationToken GetCancellationToken { get; set; }
        /// <summary>
        /// HttpContext
        /// </summary>
        HttpContext HttpContext { get; set; }
        /// <summary>
        /// WebSocket
        /// </summary>
        WebSocket WebSocket { get; set; }
        /// <summary>
        /// Session 开始时间
        /// </summary>
        DateTime StartTime { get; set; }
        /// <summary>
        /// Session 最后一次活动时间
        /// </summary>
        DateTime LastActiveTime { get; set; }
        /// <summary>
        /// 数据
        /// </summary>
        string Data { get; set; }
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetData<T>();
        /// <summary>
        /// 发送数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Data"></param>
        /// <returns></returns>
        Task SendAsync<T>(T Data);
        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        Task SendAsync(string Data);


    }
}
