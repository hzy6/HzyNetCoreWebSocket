﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NetCoreSocket.Models;

namespace NetCoreSocket.Controllers
{
    using Toolkit;

    public class HomeController : Controller
    {
        [Route("")]
        public IActionResult Main()
        {
            return View();
        }

        public IActionResult Index(string id)
        {
            ViewBag.ID = id;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            var feature = HttpContext.Features.Get<Microsoft.AspNetCore.Diagnostics.IExceptionHandlerFeature>();
            var error = feature?.Error;
            Tools.Log.Write(error, HttpContext.Connection.RemoteIpAddress.ToString());//log4net 写入日志到 txt

            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
